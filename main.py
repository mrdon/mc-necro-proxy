import asyncio
from contextlib import closing, contextmanager
import json
import logging
import os

from protocol import MinecraftProtocol
from proxy import start_proxy


games = {}

log = logging.getLogger("main")


class Game:
    def __init__(self, name):
        self.run_file = None
        self.port = -1
        self.name = name
        self._users = 0
        self.process = None
        self.status = None
        self._read_game_data()
        self.mc_lock = asyncio.Lock()

    def _read_game_data(self):
        server_conf_path = self._find_server_conf()
        with open(server_conf_path) as f:
            for line in f.readlines():
                try:
                    key, val = line.split("=")
                    if key == "server-port":
                        self.port = int(val)
                except ValueError:
                    pass
        run_sh = os.path.join(self.name, "run.sh")
        if os.path.isfile(run_sh):
            self.run_file = run_sh
        else:
            run_bat = os.path.join(self.name, "run.bat")
            if os.path.isfile(run_bat):
                self.run_file = run_bat
            else:
                raise ValueError("Missing run.sh (or run.bat) script to launch the game")

    def _find_server_conf(self):
        paths = (os.path.join(self.name, "server.properties"),
                 os.path.join(self.name, "config", "server.cfg"))
        for path in paths:
            if os.path.isfile(path):
                return path
        raise ValueError("Missing server.properties or config/server.cfg")

    @property
    def users(self):
        return self._users

    @property
    def description(self):
        if self.status:
            desc = self.status['description']
            if isinstance(desc, dict) and 'text' in desc:
                return desc['text']
            else:
                return desc
        return ''

    @users.setter
    def users(self, value):
        if self.status:
            self.status['players']['online'] = value
        self._users = value

    @asyncio.coroutine
    def load_status(self):
        with (yield from _connect_to_local(self.port)) as proto:
            yield from asyncio.sleep(2)
            self.status = yield from proto.get_status(self.name, self.port)
            log.info("Loaded status: {}".format(json.dumps(self.status)))

    @asyncio.coroutine
    def new_session(self):
        with (yield from self.mc_lock):
            if self.process is None:

                num_games = 0
                for game in games.values():
                    num_games += game.users

                if num_games > 2:
                    raise ValueError("Too many games in progress, try again later")

                log.info("Creating new minecraft: {}".format(self.name))

                self.process = yield from asyncio.create_subprocess_shell(self.run_file, stdin=asyncio.subprocess.PIPE)

                with (yield from _connect_to_local(self.port)):
                    pass

        @contextmanager
        def session():
            self.users += 1
            yield
            self.users -= 1
            with (yield from self.mc_lock):
                if self.users == 0:
                    log.info("Killing minecraft server: {}".format(self.name))
                    asyncio.async(self.process.communicate("/stop\n".encode("utf-8")))
                    self.process = None

        return session()


def get_game(name):
    global games
    if name not in games:
        games[name] = Game(name)
    return games[name]


@asyncio.coroutine
def _populate_game_statuses():
    for name in os.listdir("."):
        if os.path.isdir(name):
            try:
                game = get_game(name)
                with (yield from game.new_session()):
                    log.info("Loading game status: {}".format(name))
                    yield from game.load_status()
            except (ValueError, FileNotFoundError) as e:
                log.info("Skipping non-game dir {}: {}".format(name, e))

    log.info("Game statuses loaded")


@asyncio.coroutine
def _connect_to_local(port, **kwargs):
    """
    :rtype: MinecraftProtocol
    """
    loop = asyncio.get_event_loop()
    last_error = None
    for x in range(15):
        try:
            log.debug("Connect attempt: {}".format(x))
            reader = asyncio.StreamReader(loop=loop)
            protocol = MinecraftProtocol(reader, loop=loop, **kwargs)
            asyncio.open_connection()
            coro = loop.create_connection(lambda: protocol, 'localhost', port)
            transport, _ = yield from asyncio.wait_for(coro, 1)
            return closing(protocol)
        except Exception as e:
            last_error = e
            yield from asyncio.sleep(1)
            pass

    log.error("Cannot connect: {}".format(last_error))
    raise ValueError("Cannot connect: {}".format(last_error))


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    logging.getLogger("asyncio").setLevel(logging.WARN)
    if os.name == 'nt':
        main_loop = asyncio.ProactorEventLoop()
        asyncio.set_event_loop(main_loop)
    else:
        main_loop = asyncio.get_event_loop()

    main_loop.run_until_complete(_populate_game_statuses())
    main_loop.run_until_complete(start_proxy(get_game))

    try:
        from web import start_web
        main_loop.run_until_complete(start_web(games))
    except ImportError:
        log.warn("aiohttp not detected, so no web interface will be available")

    try:
        main_loop.run_forever()
    except KeyboardInterrupt:
        pass
