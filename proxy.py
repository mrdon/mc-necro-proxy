# !/usr/bin/env python3

import asyncio
import logging
import sys

from protocol import MinecraftProtocol, HandshakePacket


BUFFER_SIZE = 65536

log = logging.getLogger(__name__)


def client_connection_string(writer):
    return '{} -> {}'.format(
        writer.get_extra_info('peername'),
        writer.get_extra_info('sockname'))


def remote_connection_string(writer):
    return '{} -> {}'.format(
        writer.get_extra_info('sockname'),
        writer.get_extra_info('peername'))


@asyncio.coroutine
def proxy_data_task(reader, writer, connection_string):
    try:
        while True:
            buffer = yield from reader.read(BUFFER_SIZE)
            if not buffer:
                break
            writer.write(buffer)
            yield from writer.drain()
    except Exception as e:
        log.info('proxy_data_task exception {}'.format(e))
    finally:
        writer.close()
        log.info('close connection {}'.format(connection_string))


@asyncio.coroutine
def accept_client_task(get_game, client_reader, client_writer):
    client_string = client_connection_string(client_writer)
    log.info('accept connection {}'.format(client_string))
    
    protocol = MinecraftProtocol(client_reader, client_writer)
    conn_info = yield from protocol.read_packet(HandshakePacket)

    try:
        game = get_game(conn_info.address)
    except Exception as e:
        log.info('Game not found: {}: {}'.format(conn_info.address, e))
        client_writer.close()
        return

    if conn_info.is_status_next():
        yield from protocol.handle_status(game.status)
        return

    with (yield from game.new_session()):
        try:
            (remote_reader, remote_writer) = yield from asyncio.wait_for(
                asyncio.open_connection(host="localhost", port=game.port),
                timeout=1)
        except asyncio.TimeoutError:
            log.info('connect timeout')
            log.info('close connection {}'.format(client_string))
            client_writer.close()
        except Exception as e:
            log.info('error connecting to remote server: {}'.format(e))
            log.info('close connection {}'.format(client_string))
            client_writer.close()
        else:
            remote_string = remote_connection_string(remote_writer)
            log.info('connected to remote {}'.format(remote_string))

            yield from conn_info.replay(remote_writer)

            client_to_remote = asyncio.async(proxy_data_task(client_reader, remote_writer, remote_string))
            remote_to_client = asyncio.async(proxy_data_task(remote_reader, client_writer, client_string))

            yield from asyncio.wait([client_to_remote, remote_to_client])


def parse_addr_port_string(addr_port_string):
    addr_port_list = addr_port_string.rsplit(':', 1)
    return addr_port_list[0], int(addr_port_list[1])


@asyncio.coroutine
def start_proxy(get_game):

    def handle_client_task(client_reader, client_writer):
        asyncio.async(accept_client_task(get_game=get_game,
                                         client_reader=client_reader,
                                         client_writer=client_writer))

    try:
        server = yield from asyncio.start_server(handle_client_task, host="0.0.0.0", port=25565)

    except Exception as e:
        log.error('Bind error: {}'.format(e))
        sys.exit(1)

    for s in server.sockets:
        log.info('listening on {}'.format(s.getsockname()))