import asyncio
from aiohttp import web
import aiohttp_jinja2
import jinja2


@asyncio.coroutine
def start_web(games):

    @aiohttp_jinja2.template('index.html')
    @asyncio.coroutine
    def list_games(request):
        return {"games": games.values()}

    app = web.Application()
    app.router.add_route('GET', '/', list_games)
    aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader("templates"))

    loop = asyncio.get_event_loop()
    srv = yield from loop.create_server(app.make_handler(), '0.0.0.0', 8080)
    print('serving on', srv.sockets[0].getsockname())