Minecraft Necro Proxy
==============================

.. image:: https://s3.amazonaws.com/uploads.hipchat.com/10804/68273/qfPq6cixF7msuRB/upload.png

Minecraft Necro Proxy is a small application that acts as a front-end to unlimited Minecraft games.  To support unlimited games, it only starts a game when a user tries to connect to it, and then when the last one leaves, it shuts the game down.  It also limits the number of games that are running at one time to avoid running out of server resources.

Features
--------

- Let's you host infinite games on one box.
- Scales to large number of players
- Automatically starts games on-demand.
- Cleans up unused games.


Requirements
------------

- Python >= 3.4
- (optional, for web UI) aiohttp https://github.com/KeepSafe/aiohttp
- (optional, for web UI) aiohttp_jinja2 https://github.com/aio-libs/aiohttp_jinja2


License
-------

``mc-necro-proxy`` is offered under the Apache 2 license.


Getting started
---------------

Server Installation
^^^^^^

On a server that has Python 3.4, clone the project to a directory::

  git clone https://bitbucket.org/mrdon/mc-necro-proxy.git

You can either run the application directly::

  python3 main.py

or start it via a process manager such as supervisord.

Install Games
^^^^^^

To install an existing Minecraft server game, copy the server directory into a subdirectory of your mc-necro-proxy
installation, named the same as the host name by which it will be accessed.  For example, if your server will be
available at ``myserver.example.com``, choose that as the name of your game directory.

Next, create a ``run.sh`` script in your game directory that will start the Minecraft server.  For example, here is
simple script for a basic instance::

  #!/bin/bash

  DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
  cd $DIR
  java -Xmx1024M -Xms1024M -jar minecraft_server.jar nogui

Finally, make sure your ``server.properties`` has ``server-port`` set to a unique port, relative to other games, ideally not accessible via the internet.

Therefore, your installation when finished should look something like::

  mc-necro-proxy/
  mc-necro-proxy/main.py
  mc-necro-proxy/[the rest of mc-necro-proxy]
  mc-necro-proxy/myserver.example.com/
  mc-necro-proxy/myserver.example.com/run.sh
  mc-necro-proxy/myserver.example.com/server.properties
  mc-necro-proxy/myserver.example.com/[the rest of the minecraft server files]

Now, start mc-necro-proxy and it should detect and load your new game.

(Optional) Set up DNS
^^^^^^

To easily define a unique host name for each Minecraft game, you can purchase or use an existing domain and set up a
`wildcard DNS record <http://en.wikipedia.org/wiki/Wildcard_DNS_record>`_.  This will allow you to serve new games with
no DNS overhead.

The general steps for setting this up are as follows:

1. Purchase a domain on https://namecheap.com, say ``example.com``.
2. Visit the admin area and create a wildcard DNS record as described in `their docs <https://www.namecheap.com/support/knowledgebase/article.aspx/9191/29/how-to-create-wildcard-subdomain-in-cpanel>`_.
3. Install your games as subdirectories that match the new domain, so ``game1.example.com`` or ``game2.example.com``.